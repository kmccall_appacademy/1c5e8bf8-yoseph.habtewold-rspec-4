class Timer
  attr_accessor :seconds

  def initialize
    @seconds = 0
  end

  def time_string
    time = convert_time
    beutify(time[0]) + ':' + beutify(time[1]) + ':' + beutify(time[2])
  end

  def convert_time
    sec = @seconds % 60
    min = (@seconds / 60) % 60
    hr = @seconds / (60 * 60)
    [hr, min, sec]
  end

  def beutify(digit)
    digit < 10 ? "0#{digit}" : "#{digit}"
  end
end
