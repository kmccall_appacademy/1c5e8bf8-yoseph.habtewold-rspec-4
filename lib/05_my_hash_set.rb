class MyHashSet
  attr_reader :store

  def initialize
    @store = {}
  end

  def insert(el)
    @store[el]  = true
  end

  def include?(el)
    @store.has_key? el
  end

  def delete(el)
    flag = @store.delete(el)
    flag ? true : false
  end

  def to_a
    @store.keys
  end

  def union(set2)
    new_set = MyHashSet.new
    self.to_a.each { |k| new_set.insert(k) }
    set2.to_a.each { |k| new_set.insert(k) }
    new_set
  end

  def intersect(set2)
    new_set = MyHashSet.new
    self.to_a.each {|k| new_set.insert(k) if set2.include?(k)}
    new_set
  end

  def minus(set2)
    new_set = MyHashSet.new
    self.to_a.each {|k| new_set.insert(k) unless set2.include?(k)}
    new_set
  end

  def symmetric_difference(set2)
    new_set = MyHashSet.new
    self.to_a.each {|k| new_set.insert(k) unless set2.include?(k)}
    set2.to_a.each {|k| new_set.insert(k) unless self.include?(k)}
    new_set
  end

  def ==(object)
    object.class == MyHashSet && self.store == object.store
  end
end
